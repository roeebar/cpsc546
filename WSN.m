function WSN (X,k,L,SNR)
    %% Inputs
    % X - sensors data
    % k - number of active sensors per time frame (sparsity)
    % L - number of bits per time frame
    % SNR - channel noise. If ommited, clean channel
    
    %% Set parameters
    addpath 'spgl1-1.8';
    N=size(X,1);
    T=size(X,2)/L;
    opts = spgSetParms('verbosity',0); % Turn off the SPGL1 log output
    lambda=k/N; % Bernoulli trial success parameter
    delta=sqrt(2)-1;
    C=0.22;
    Mc=ceil(2*C*delta^(-2)*k*log(N/k)) % number of channels
    if (nargin>3)
        sigma=sqrt(k/10^(SNR/10)/2/Mc); % noise std based on SNR
    else
        sigma=0;
    end
    epsilon=sqrt(Mc)*sigma;; % Basis pursuit denoising epsilon  
    h=1/2; % QPSK decision radius
    e1=[];
    e2=[];
    e3=[];
    q=[];

    %% Generate the sensing matrix
    %rand('twister',0); randn('state',2);
    A = randn(N,Mc)/sqrt(Mc);               % Random encoding matrix with orthogonal rows
    A=A'; % A is McxN matrix    

    %% Setup the constellation plot 
    figure;
    hold on;
    axis (1/sqrt(2)*[-1.25,1.25 -1.25 1.25]);
    axis manual;
    xlabel('Real');
    ylabel('Imagin');
    title('Constellation Diagram');
    plot((-1.25:0.1:1.25)*1/sqrt(2),0*(-1.25:0.1:1.25)*1/sqrt(2),'k--');
    plot(0*(-1.25:0.1:1.25)*1/sqrt(2),(-1.25:0.1:1.25)*1/sqrt(2),'k--');
    plot(h*cos(0:pi/50:2*pi),h*sin(0:pi/50:2*pi),'k--');
   
    %% Simulate time
    for j=1:T
        % Bernoulli trial to decide which sensors are active
        b=rand(N,1)<lambda;
        for m=1:L/2
            y1=randn(Mc,1)*sigma;
            y2=randn(Mc,1)*sigma;
            x1=X(:,(j-1)*L+2*m-1);
            x2=X(:,(j-1)*L+2*m);
            % Convert active sensors data from binary to symbols
            s1=(double(x1)-0.5)*sqrt(2).*b; 
            s2=(double(x2)-0.5)*sqrt(2).*b;
            for n=1:N
                [d1, d2]=simulate_sensor(A(:,n),[s1(n) s2(n)]);
                y1=y1+d1;
                y2=y2+d2;
            end   

            %% Recover the sensors data
            if (epsilon~=0)
                r1 = spg_bpdn(A, y1, epsilon, opts);
                r2 = spg_bpdn(A, y2, epsilon, opts);
            else
                r1 = spg_bp(A, y1, opts);
                r2 = spg_bp(A, y2, opts);
            end
            q(end+1)=sum(b);
            e1(end+1)=norm([s1;s2] - [r1;r2]);

            %% Decision between 0,1
            a=(r1.^2+r2.^2)>h^2;
            z1=r1(a)>0;
            z2=r2(a)>0;
            e2(end+1)=norm(double([x1(a);x2(a)])-[z1;z2],1);
            e3(end+1)=norm(a-b,1);
            scatter(r1(a), r2(a),'ob');
            scatter(r1(~a), r2(~a),'or');
            scatter(s1, s2,'xg');

        end
    end
    
    %% Plotting error
    figure;
    subplot(3,1,1);
    stem((0:length(e1)-1)/L*2,e1,'MarkerSize',3.75);
    title('Recovered Symbol Error');
    xlabel('Time frame');
    ylabel('|s-z|(t)');
    subplot(3,1,2);
    stem((0:length(e2)-1)/L*2,e2,'MarkerSize',3.75);
    title('Recovered Binary Numbers Error');
    xlabel('Time frame');
    ylabel('|x-z|(t)');
    subplot(3,1,3);
    stem((0:length(e3)-1)/L*2,e3,'MarkerSize',3.75);
    title('Recovered Which Sensors are Active Error');
    xlabel('Time frame');
    ylabel('|a-b|(t)');
    
    %% Plotting active sensors
    figure;
    plot((0:length(q)-1)/L*2,q);
    title('Number of Active Sensors');
    ylim([0,N]);
    xlabel('Time frame');
    ylabel('Active sensors');
end

function [d1, d2] = simulate_sensor(v,s)
    P=1;
    s1=s(1);s2=s(2);
    d1=P*s1*v;
    d2=P*s2*v;
end