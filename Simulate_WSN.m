%% Load sensors data
load('WSN_Data.mat');

%% Generata data stream
N=128; % number of sensors
T=2; % simulation run time
L=32; % 32-bits per measurement
k=20; % sparsity requirement
X=(Data(1:N,1:T*L)); % each row is a sensor data stream
SNR=24; % SNR in db

%% Simulate WSN
WSN(X,k,L,SNR);
